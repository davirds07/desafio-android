package com.davi.daviroidrigues.desafioandroid.ScrollPagination;

import android.widget.AbsListView;

public class OnScrollPagination implements AbsListView.OnScrollListener{

    private OnLoadMoreListener loadMoreListener;
    private int visibleShold = 3;
    private int currentPage = 0;
    private int currentTotalItems = 0;
    private int firstItemPageIndex = 1;
    private boolean loading = false;


    public OnScrollPagination(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount < currentTotalItems) {
            this.currentPage = this.firstItemPageIndex;
            this.currentTotalItems = totalItemCount;
            if (totalItemCount == 0) { this.loading = true; }
        }

        if (loading && (totalItemCount > currentTotalItems)) {
            loading = false;
            currentTotalItems = totalItemCount;
            currentPage++;
        }

        if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem + visibleShold)) {
            loadMoreListener.onLoadMore(currentPage + 1, totalItemCount);
            loading = true;
        }
    }
}
