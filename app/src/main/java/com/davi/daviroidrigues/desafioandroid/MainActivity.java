package com.davi.daviroidrigues.desafioandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.davi.daviroidrigues.desafioandroid.Models.PageModel;
import com.davi.daviroidrigues.desafioandroid.Models.ShotsModel;
import com.davi.daviroidrigues.desafioandroid.ScrollPagination.OnLoadMoreListener;
import com.davi.daviroidrigues.desafioandroid.ScrollPagination.OnScrollPagination;
import com.davi.daviroidrigues.desafioandroid.api.DribbbleService;
import com.davi.daviroidrigues.desafioandroid.baseadapters.ListAdapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.RestAdapter;


public class MainActivity extends ActionBarActivity implements OnLoadMoreListener {

    public ListView listViewShot;
    public ListAdapter adapter;
    public static final String END_POINT = "http://api.dribbble.com";
    public int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ListAdapter(getBaseContext());

        listViewShot = (ListView) findViewById(R.id.listViewShot);

        listViewShot.setOnScrollListener(new OnScrollPagination(MainActivity.this));

        listViewShot.setAdapter(adapter);

        this.listViewShot.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getBaseContext(),ShotActivity.class);
                // Passa os dados da tela atual para a proxima.
                i.putExtra("id", id)
                .putExtra("image_url", adapter.getItem(position).getImage_url())
                .putExtra("title", adapter.getItem(position).getTitle())
                .putExtra("views_count", adapter.getItem(position).getViews_count())
                .putExtra("description", adapter.getItem(position).getDescription())
                .putExtra("avatar_url", adapter.getItem(position).getPlayer().getAvatar_url())
                .putExtra("username", adapter.getItem(position).getPlayer().getUsername());
                startActivity(i);
            }

        });


    }

    @Override
    public void onLoadMore(int page, final int totalItemsCount) {

        currentPage = page;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    if (currentPage <= PageModel.getPages()) {
                        RestAdapter restAdapter = new RestAdapter.Builder()
                                .setEndpoint(END_POINT)
                                .setLogLevel(RestAdapter.LogLevel.FULL)
                                .build();

                        DribbbleService dribbble = restAdapter.create(DribbbleService.class);

                        PageModel page = dribbble.getPage(currentPage);

                        List<ShotsModel> shots = page.getShots();

                        // Ordena pelo mais visualizado.
                        Collections.sort(shots, new Comparator<ShotsModel>() {
                            @Override
                            public int compare(ShotsModel shot1, ShotsModel shot2) {
                                return (shot1.getViews_count() > shot2.getViews_count() ? -1 :
                                        (shot1.getViews_count() == shot2.getViews_count() ? 0 : 1));
                            }
                        });
                        adapter.setListShots(shots);
                    } else {
                        Toast.makeText(getBaseContext(), "No have more shots.", Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e ) {

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
