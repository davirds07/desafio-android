package com.davi.daviroidrigues.desafioandroid;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.davi.daviroidrigues.desafioandroid.Models.ShotsModel;
import com.davi.daviroidrigues.desafioandroid.api.DribbbleService;
import com.squareup.picasso.Picasso;

import retrofit.RestAdapter;


public class ShotActivity extends Activity {

    public int id;
    public ShotsModel shot;
    public ImageView image_url;
    public TextView title;
    public TextView views_count;
    public ImageView avatar_url;
    public TextView username;
    public TextView description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot);

        image_url = (ImageView) findViewById(R.id.image_url);
        title = (TextView) findViewById(R.id.title);
        views_count = (TextView) findViewById(R.id.views_count);
        avatar_url = (ImageView) findViewById(R.id.avatar_url);
        username = (TextView) findViewById(R.id.username);
        description = (TextView) findViewById(R.id.description);

        // Recebe os dados
        Intent i = getIntent();
        id = i.getExtras().getInt("id");
        title.setText(i.getExtras().getString("title"));
        Picasso.with(getBaseContext())
        .load(i.getExtras().getString("image_url"))
        .into(image_url);
        views_count.setText(String.valueOf(i.getExtras().getInt("views_count")));
        username.setText(i.getExtras().getString("username"));
        Picasso.with(getBaseContext())
        .load(i.getExtras().getString("avatar_url"))
        .into(avatar_url);
        description.setText(Html.fromHtml(i.getExtras().getString("description")));
    }

}
