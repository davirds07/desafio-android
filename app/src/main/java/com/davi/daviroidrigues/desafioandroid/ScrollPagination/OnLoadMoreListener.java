package com.davi.daviroidrigues.desafioandroid.ScrollPagination;

public interface OnLoadMoreListener {

    public void onLoadMore(int page, int totalItemsCount);
}


