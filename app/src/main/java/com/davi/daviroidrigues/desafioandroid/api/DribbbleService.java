package com.davi.daviroidrigues.desafioandroid.api;

import com.davi.daviroidrigues.desafioandroid.Models.PageModel;
import com.davi.daviroidrigues.desafioandroid.Models.ShotsModel;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;


/**
 * Responsavel por gerenciar todo o acesso a api do Dribble
 */
public interface DribbbleService {

    /**
     * Executa requisição get para buscar as paginas
     *
     * @param page
     */
    @GET("/shots/popular?page=1")
    PageModel getPage(@Query("page") int page);

}
