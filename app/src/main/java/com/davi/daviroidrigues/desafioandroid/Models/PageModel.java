package com.davi.daviroidrigues.desafioandroid.Models;

import java.util.List;

/**
 * Responsavel por cuidar dos dados da pagina
 */
public class PageModel {

    private int page;
    private int per_page;
    protected static int pages = 50;
    protected static int total = 750;
    private List<ShotsModel> shots;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public List<ShotsModel> getShots() {
        return shots;
    }

    public void setShots(List<ShotsModel> shots) {
        this.shots.addAll(shots);
    }

    public static int getPages() {
        return pages;
    }

    public static int getTotal() {
        return total;
    }
}
