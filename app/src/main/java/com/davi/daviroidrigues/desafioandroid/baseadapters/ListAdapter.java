package com.davi.daviroidrigues.desafioandroid.baseadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.davi.daviroidrigues.desafioandroid.Models.ShotsModel;
import com.davi.daviroidrigues.desafioandroid.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daviroidrigues on 30/01/15.
 */
public class ListAdapter extends BaseAdapter {

    public Context context;
    public List<ShotsModel> listShots = new ArrayList<>();

    public ListAdapter(Context context){
        this.context = context;
    }

    public void setListShots(List<ShotsModel> listShots){
        this.listShots.addAll(listShots);
    }

    @Override
    public int getCount() {
        return this.listShots.size();
    }

    @Override
    public ShotsModel getItem(int position) {
        return listShots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listShots.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shot_view, parent, false);
        }

        ImageView image_url = (ImageView) convertView.findViewById(R.id.image_url);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView views_count = (TextView) convertView.findViewById(R.id.views_count);

        ShotsModel shot = this.getItem(position);

        Picasso.with(this.context)
        .load(shot.getImage_url())
        .into(image_url);

        title.setText(shot.getTitle());
        views_count.setText(String.valueOf(shot.getViews_count()));

        return convertView;

    }
}
